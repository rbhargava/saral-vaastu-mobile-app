// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'UB_PRIDE_APP' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'UB_PRIDE_APP.controllers' is found in controllers.js
angular.module('UB_PRIDE_APP', ['ionic', 'UB_PRIDE_APP.controllers', 'UB_PRIDE_APP.services', 'UB_PRIDE_APP.SFDC', 'ngSanitize','uiGmapgoogle-maps','angularjs-dropdown-multiselect'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
       /* if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }*/
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "templates/menu.html",
            controller: 'AppCtrl'
        })

    .state('app.notifications', {
        url: "/notifications",
        views: {
            'menuContent': {
                templateUrl: "templates/notifications.html",
                controller: 'NotificationsCtrl'
            }
        }
    })

    .state('app.today', {
            url: "/today",
            views: {
                'menuContent': {
                    templateUrl: "templates/today.html",
                    controller: 'TodayCtrl'
                }
            }
        })
        .state('app.map', {
            url: "/map",
            views: {
                'menuContent': {
                    templateUrl: "templates/map.html",
                    controller: 'MapCtrl'

                }
            }
        })
        .state('app.products', {
            url: "/products",
            views: {
                'menuContent': {
                    templateUrl: "templates/products.html"
                }
            }
        })

    .state('app.reports', {
            url: "/reports",
            views: {
                'menuContent': {
                    templateUrl: "templates/reports.html"
                }
            }
        })
        .state('app.chatter', {
            url: "/chatter",
            views: {
                'menuContent': {
                    templateUrl: "templates/chatter.html"

                }
            }
        })
        .state('app.overview', {
            url: "/overview/:visitId",
            views: {
                'menuContent': {
                    templateUrl: "templates/overview.html",
                    controller: "OverviewCtrl"
                }
            }
        })

    .state('app.stockAndSales', {
            url: "/stockAndSales/:visitId",
            views: {
                'menuContent': {
                    templateUrl: "templates/stockAndSales.html",
                     controller: "StockAndSalesCtrl"
                }
            }
        })
        .state('app.cooler', {
            url: "/cooler/:visitId",
            views: {
                'menuContent': {
                    templateUrl: "templates/cooler.html",
                     controller: "CoolerCtrl"                   

                }
            }
        })
        .state('app.popAndVisibility', {
            url: "/popAndVisibility",
            views: {
                'menuContent': {
                    templateUrl: "templates/popAndVisibility.html"
                }
            }
        })
        .state('app.promos', {
            url: "/promos",
            views: {
                'menuContent': {
                    templateUrl: "templates/promos.html"

                }
            }
        })
        .state('app.competitor', {
            url: "/competitor",
            views: {
                'menuContent': {
                    templateUrl: "templates/competitor.html"
                }
            }
        })

    .state('app.feedback', {
            url: "/feedback",
            views: {
                'menuContent': {
                    templateUrl: "templates/feedback.html"
                }
            }
        })
        .state('app.home', {
            url: "/home",
            views: {
                'menuContent': {
                    templateUrl: "templates/notifications.html",
                    controller: 'NotificationsCtrl'

                }
            }
        })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/notifications');
});