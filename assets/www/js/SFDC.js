angular.module('UB_PRIDE_APP.SFDC', [])

.factory('SalesforceSession', ['$rootScope', 'cordovaReady', function($rootScope, cordovaReady) {
    return {
        refreshSalesforceSession: cordovaReady(function(onSuccess, onError) {
            cordova.require("salesforce/plugin/oauth").getAuthCredentials(function() {
                var that = this,
                    args = arguments;
                if (onSuccess) {
                    $rootScope.$apply(function() {
                        onSuccess.apply(that, args);
                    });
                }
            }, function() {
                var that = this,
                    args = arguments;
                if (onError) {
                    $rootScope.$apply(function() {
                        onError.apply(that, args);
                    });
                }
            });
        })
    };
}])

// LOCAL DB SOUPS

.factory('SOUP_REGISTER', ['$q', '$log', function($q, $log) {

      var handleRequest = function(tableName, indexSpec) {
            var defer = $q.defer();
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
            var querySpec;
            sfSmartstore.registerSoup(tableName, indexSpec, function(response) {
                 $log.debug(tableName+' Soup created'); 
                defer.resolve(response);
            }, function(error) {
                defer.reject(error);
            });
            return defer.promise;
        }

        return {
            registerSoup: function(tableName, indexSpec) {
                return handleRequest(tableName, indexSpec);
            }
        }
      
}])
.factory('REST_API', ['$q', '$log', function($q, $log) {
    return function(path, method, payload, headerParams){
        var defer=$q.defer();
        var cordovaRe = cordova.require("salesforce/plugin/oauth");
        cordovaRe.getAuthCredentials(function(creds) {
            var apiVersion = "v29.0";
            var credsData = creds;
            if (creds.data) // Event sets the data object with the auth data.
                credsData = creds.data;
            var forcetkClient = new forcetk.Client(credsData.clientId, credsData.loginUrl, null, cordova.require("salesforce/plugin/oauth").forcetkRefresh);
            forcetkClient.setSessionToken(credsData.accessToken, apiVersion, credsData.instanceUrl);
            forcetkClient.setRefreshToken(credsData.refreshToken);
            forcetkClient.setUserAgentString(credsData.userAgent);
            forcetkClient.apexrest(path, method, payload, headerParams, function(response) {
               // $log.debug('onSuccess'+response);
                 defer.resolve(response);
            },function(error){
                $log.debug('onError'+error);
                defer.resolve(error);
            });

        });
        return defer.promise;
    }
}])
.factory('FETCH_OBJECT_RECORDS', ['$q', '$log','REST_API','SOUP_EXISTS','REMOVE_SOUP','SOUP_REGISTER','MOBILEDATABASE_ADD', function($q, $log,REST_API,SOUP_EXISTS,REMOVE_SOUP,SOUP_REGISTER,MOBILEDATABASE_ADD) {
    return function(){
        var defer=$q.defer();
        $q.all({
            OBJECTS_RECORDS: REST_API("/FetchObjectsSoupsAndRecordsServiceAPI", 'GET', null, null)
        }).then(function(response){
            $log.debug('dadafasdsad'+JSON.stringify(response));
             angular.forEach(response.OBJECTS_RECORDS, function(record, key) {
                 var flag=SOUP_EXISTS.soupExists(record.ObjectName);
                 if(flag==false){
                    SOUP_REGISTER.registerSoup(record.ObjectName, record.Fields);
                    $log.debug('rachitt');
                 }
                 MOBILEDATABASE_ADD.addDataToDatabase(record.ObjectName, record.Records);
               
            });
            defer.resolve('Success');
        },function(error){
           //  $log.debug('dadafasdsad'+JSON.stringify(error));
             defer.resolve('Failure');
        });
        return defer.promise;
    }
}])    
.factory('FETCH_MOBILE_SETTINGS', ['$q', '$log','REST_API','SOUP_EXISTS','REMOVE_SOUP','SOUP_REGISTER','MOBILEDATABASE_ADD', function($q, $log,REST_API,SOUP_EXISTS,REMOVE_SOUP,SOUP_REGISTER,MOBILEDATABASE_ADD) {
    return function(){
       var defer=$q.defer();
        /*var DB_PAGE = "DB_page";
        var DB_SECTION = "DB_section";
        var DB_FIELDS = "DB_fields";
        
       $q.all({
            MOBILE_SETTINGS_MAP: REST_API("/MobileConfigurationServiceAPI", 'GET', null, null)
        }).then(function(resp) {
            var mobileSettings = resp.MOBILE_SETTINGS_MAP; 
            var flag=SOUP_EXISTS.soupExists(DB_PAGE);
            //$log.debug('flag==='+JSON.stringify(flag));     
            if (flag==true) {
                $log.debug('soupExists');
                REMOVE_SOUP.removeSoup(DB_PAGE);
                REMOVE_SOUP.removeSoup(DB_SECTION);
                REMOVE_SOUP.removeSoup(DB_FIELDS);
                $log.debug('Soups are removed');
            }
            SOUP_REGISTER.registerSoup(DB_PAGE, mobileSettings.MobilePage);
            MOBILEDATABASE_ADD.addDataToDatabase(DB_PAGE, mobileSettings.MobilePageRecords);
            SOUP_REGISTER.registerSoup(DB_SECTION, mobileSettings.MobilePageSection);  
            MOBILEDATABASE_ADD.addDataToDatabase(DB_SECTION, mobileSettings.MobilePageSectionRecords);   
            SOUP_REGISTER.registerSoup(DB_FIELDS, mobileSettings.MobilePageSectionFields);
            MOBILEDATABASE_ADD.addDataToDatabase(DB_FIELDS, mobileSettings.MobilePageSectionFieldsRecords);
            defer.resolve('Success');
        },function(error){
            $log.debug('on error'+JSON.stringify(error));
            defer.resolve('Failure');
        });*/
        return defer.promise;
    };
}])
.factory('FETCH_DATA_LOCAL', ['$q', '$log', function($q, $log) {
        return function(tableName,querySpec) {           
            var result;
            var sfSmartstore = cordova.require("salesforce/plugin/smartstore");            
            sfSmartstore.runSmartQuery(querySpec, function(response) {           
                result=response;
            }, function(error) {
                $log.debug('error'+error);
                result=error;               
            });
            return result;
        }
    }])

.factory("MOBILEDATABASE_ADD", ['$q', '$log', function($q, $log) {
    var handleRequest = function(tableName, datalist) {
        var defer = $q.defer();
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        sfSmartstore.upsertSoupEntriesWithExternalId(tableName, datalist, 'Id', function(response) {
            $log.debug(tableName+' records are inserted locally');   
            defer.resolve(response);
        }, function(error) {
            $log.debug("insert error"+JSON.stringify(error));
            defer.reject(error);
        });

        return defer.promise;
    }
    return {
        addDataToDatabase: function(tableName, datalist) {

            return handleRequest(tableName, datalist);
        }
    }
}])

// REMOVE SOUPS

.factory('REMOVE_SOUP', ['$q', '$log', function($q, $log) {
    var handleRequest = function(SOUP_NAME) {
        var defer = $q.defer();
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        sfSmartstore.removeSoup(SOUP_NAME, function(response) {
         //   $log.debug('on Success'+JSON.stringify(response));
            defer.resolve(response);
        }, function(error) {
            $log.debug('on error'+JSON.stringify(error));
            defer.reject(error);
        });
        return defer.promise;
    }
    return {
        removeSoup: function(SOUP_NAME) {
            return handleRequest(SOUP_NAME);
        }

    }
}])

.factory('SOUP_EXISTS', ['$q', '$log', function($q, $log) {
    var handleRequest = function(SOUP_NAME) {
        var defer = $q.defer();
        var flag=false;
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        sfSmartstore.soupExists(SOUP_NAME, function(response) {
           // $log.debug(SOUP_NAME+'on Success'+JSON.stringify(response));
            if(response==true){
                flag=true;
            }
            //defer.resolve('Success');
        }, function(error) {
            $log.debug(SOUP_NAME+'on error'+JSON.stringify(error));
           // defer.reject('error');
        });
        return flag;
    }
    return {
        soupExists: function(SOUP_NAME) {
            return handleRequest(SOUP_NAME);
        }
    }
}])
.factory('FETCH_DAILY_PLAN_ACCOUNTS', ['$q', '$log','$filter','FETCH_DATA_LOCAL', 'SOUP_EXISTS','FETCH_DATA', function($q, $log,$filter,FETCH_DATA_LOCAL, SOUP_EXISTS,FETCH_DATA){
    return function(){
        /*$log.debug('rachit');
        var TodaysPlan=[];
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        var today = $filter('date')(new Date(), 'yyyy-MM-dd');
        var queryVariable = ''; 
        var routePlanMap=[];
        $log.debug('rachit1');
        
        var todayAccounts = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit__c:_soup} FROM {Visit__c} ", 100));

         angular.forEach(todayAccounts.currentPageOrderedEntries, function(record, key) {
            $log.debug('rachit'+JSON.stringify(record));
            var rec=record[0];            
            TodaysPlan.push(record);
            });
            return TodaysPlan;*/
         }   
        
 }])
.factory('FETCH_DATA', ['$q','$log', function($q,$log,SOUP_EXISTS){
    var handleRequest = function(tableName,indexSpec,indexValue){
    var result;
    var sfSmartstore = cordova.require("salesforce/plugin/smartstore");

   var querySpec = sfSmartstore.buildAllQuerySpec(indexSpec, indexValue);
    if(indexValue != null){
        querySpec = sfSmartstore.buildExactQuerySpec(indexSpec,indexValue);
    }
    sfSmartstore.querySoup(tableName,querySpec,function(response){
     result=response;
    },function(error){
      result=error;
    });
    return result;
 }
     return {
        querySoup: function(tableName,indexSpec,indexValue) {
            return handleRequest(tableName,indexSpec,indexValue);
        }
    }
}]);