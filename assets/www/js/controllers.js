angular.module('UB_PRIDE_APP.controllers', [])

.controller('AppCtrl', function($scope, $rootScope, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession) {
        $scope.LeftMenu = true;
        $scope.MenuList1 = [{
            Name: 'NOTIFICATIONS',
            Url: 'notifications',
            imageUrl: 'LDPI-32/32x32-12.png'
        }, {
            Name: 'TODAY',
            Url: 'today',
            imageUrl: 'LDPI-32/32x32-04.png'
        }];
        $scope.MenuList2 = [{
            Name: 'OVERVIEW',
            Url: 'overView',
            imageUrl: ''
        }, {
            Name: 'STOCK AND SALES',
            Url: 'stockandSales',
            imageUrl: ''
        }, {
            Name: 'COOLER',
            Url: 'cooler',
            imageUrl: ''
        }, {
            Name: 'POP VISIBILITY',
            Url: 'popAndVisibility',
            imageUrl: ''
        }, {
            Name: 'PROMOS',
            Url: 'promos',
            imageUrl: ''
        }, {
            Name: 'COMPETITOR',
            Url: 'competitor',
            imageUrl: ''
        }, {
            Name: 'FEEDBACK',
            Url: 'feedback',
            imageUrl: ''
        }, {
            Name: 'HOME',
            Url: 'home',
            imageUrl: ''
        }];
        $scope.checkConnection = function() {
            // $log.debug('navigator===='+JSON.stringify(navigator));
            if (navigator && (navigator.connection && navigator.connection.type != 'none') || (navigator.smartstore != undefined)) {
                $scope.online = true;
            } else {
                $scope.online = false;
            }
        };
        $scope.changeMenu = function(menu) {
            if (menu === 'Menu1') {
                $scope.Menus = $scope.MenuList1;
            } else {
                $scope.Menus = $scope.MenuList2;
            }
        };
        
         $scope.distance = function(lat1, lon1, lat2, lon2, unit) {
            var radlat1 = Math.PI * lat1 / 180;
            var radlat2 = Math.PI * lat2 / 180;
            var radlon1 = Math.PI * lon1 / 180;
            var radlon2 = Math.PI * lon2 / 180;
            var theta = lon1 - lon2;
            var radtheta = Math.PI * theta / 180;
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit == "K") {
                dist = dist * 1.609344;
            }
            if (unit == "N") {
                dist = dist * 0.8684;
            }
            return dist
        };
        


    })
    .controller('NotificationsCtrl', function($scope, $rootScope, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_MOBILE_SETTINGS, FETCH_OBJECT_RECORDS, FETCH_DAILY_PLAN_ACCOUNTS, FETCH_DATA_LOCAL) {
        $ionicLoading.show({
            template: 'Loading......'
        });
        $scope.changeMenu('Menu1');
        $scope.isSuccessSettings = false;
        $scope.isSuccessRecords = false;
        $scope.checkConnection();
        $rootScope.Uploads = []
        if ($scope.online == true) {
            $q.all({
                IS_SETTINGS_SUCCESS: FETCH_MOBILE_SETTINGS(),
                IS_RECORDS_SUCCESS: FETCH_OBJECT_RECORDS()
            }).then(function(response) {
                $scope.isSuccessSettings = response.IS_SETTINGS_SUCCESS;
                $scope.isSuccessRecords = response.IS_RECORDS_SUCCESS;
            });
        }
        $ionicLoading.hide();
            })
    .controller('TodayCtrl', function($scope, $rootScope, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL) {
            $ionicLoading.show({
            template: 'Loading Today List.......'
        });
        var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
        $scope.TodaysPlan=[];
        $scope.TomorrowsPlan = [];
        $scope.WeeklyPlan =[];
        var today = new Date();
        
        var d = new Date();
        var d1 = new Date();
        var tomorrow = new Date(d.setDate(d.getDate() + 1));
        var nxt7day = new Date(d1.setDate(d1.getDate() + 7));
        //alert(today+' '+tomorrow);
        //alert(today);
           $log.debug('rachit0');     
          var todayDate = $filter('date')(today, 'yyyy-MM-dd');
          var tomorrowDate = $filter('date')(tomorrow, 'yyyy-MM-dd');
          var sevenPlusDay = $filter('date')(nxt7day, 'yyyy-MM-dd');
          //alert(todayDate);
          //alert(tomorrowDate);
          var todayAccounts = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit__c:_soup} FROM {Visit__c} ", 100));
          angular.forEach(todayAccounts.currentPageOrderedEntries, function(record, key) {
            var rec=record[0];
            $log.debug('rachit1' + record[0].VisitDateHidden__c);  
            $log.debug('rachit2');    
            $log.debug(record[0].VisitDateHidden__c);
            if (record[0].VisitDateHidden__c != undefined && record[0] != '') {
                $log.debug('rachit3');

                if (record[0].VisitDateHidden__c === todayDate) {
                    $log.debug('rachit4');    
                    $scope.TodaysPlan.push(rec);
                }
                else if(record[0].VisitDateHidden__c >= tomorrowDate && record[0].VisitDateHidden__c <= sevenPlusDay) {
                    $log.debug('rachit5');    
                    $scope.WeeklyPlan.push(rec);
                    if(record[0].VisitDateHidden__c === tomorrowDate){
                        $log.debug('rachit6');
                        $scope.TomorrowsPlan.push(rec);
                    }
                }
                
                $log.debug('rachit7'+JSON.stringify(rec));
            }   
        })

         
        $log.debug('rachit7');    
        $ionicLoading.hide();
            })

.controller('OverviewCtrl', function($scope, $stateParams, $window, $location, $rootScope, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, FETCH_DATA, MOBILEDATABASE_ADD) {
    $scope.myVisit=$stateParams.visitId;
    $scope.currentVisit = [];
    $scope.visOpp = [];
    $scope.visProp = [];
    $scope.visAcc = [];
    $scope.oppId;
    $scope.propId;
    $scope.accId;
    var sfSmartstore = cordova.require("salesforce/plugin/smartstore");
    var wherVar = "{Visit__c:Id} = '"+$stateParams.visitId+"'";
    //alert(wherVar);
    var currVis = FETCH_DATA_LOCAL('Visit__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Visit__c:_soup} FROM {Visit__c} WHERE " + wherVar , 100));
    angular.forEach(currVis.currentPageOrderedEntries, function(record, key) {
        var rec=record[0];
        $scope.oppId = record[0].Opportunity__c;
        $scope.currentVisit.push(rec);
    })
    var wherOpp = "{Opportunity:Id} = '"+$scope.oppId+"'";
    var currOpp = FETCH_DATA_LOCAL('Opportunity', sfSmartstore.buildSmartQuerySpec("SELECT  {Opportunity:_soup} FROM {Opportunity} WHERE " + wherOpp , 100));
    angular.forEach(currOpp.currentPageOrderedEntries, function(record, key) {
        var rec=record[0];
        $scope.propId = record[0].Property__c;
        $scope.accId = record[0].AccountId;
        $scope.visOpp.push(rec);
    })
    var wherProp = "{Property__c:Id} = '"+$scope.propId+"'";
    var currProp = FETCH_DATA_LOCAL('Property__c', sfSmartstore.buildSmartQuerySpec("SELECT  {Property__c:_soup} FROM {Property__c} WHERE " + wherProp , 100));
    angular.forEach(currProp.currentPageOrderedEntries, function(record, key) {
        var rec=record[0];
        $scope.visProp.push(rec);
    })
    var wherAcc = "{Account:Id} = '"+$scope.accId+"'";
    var currAcc = FETCH_DATA_LOCAL('Account', sfSmartstore.buildSmartQuerySpec("SELECT  {Account:_soup} FROM {Account} WHERE " + wherAcc , 100));
    angular.forEach(currAcc.currentPageOrderedEntries, function(record, key) {
        var rec=record[0];
        $scope.visAcc.push(rec);
    })
    })

    .controller('MapCtrl', function($scope, $stateParams, $rootScope, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS) {
    })
    .controller('StockAndSalesCtrl', function($scope, $stateParams, $rootScope, $window, $state, $location, $q, $ionicModal, $timeout, $interval, $ionicLoading, $log, $filter, SalesforceSession, FETCH_DATA_LOCAL, SOUP_EXISTS, FETCH_DAILY_PLAN_ACCOUNTS, MOBILEDATABASE_ADD) {
})
    .controller('CoolerCtrl', function() {

    });
    